package com.g_gent.sumg.greengent.weeklyreport

import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Resources
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.ActionBarActivity
import android.view.Menu
import android.view.MenuItem


public class MainActivity : ActionBarActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.container, MainFragment()).commit()
        }

        onFirstLaunch()
    }


    fun onFirstLaunch(){

        val res = getResources() as Resources
        val preferences = PreferenceManager.getDefaultSharedPreferences(this) as SharedPreferences
        val employee_number = preferences.getString(res.getString(R.string.pref_key_employee_number), "")
        val last_name = preferences.getString(res.getString(R.string.pref_key_employee_name_sei), "")
        val first_name = preferences.getString(res.getString(R.string.pref_key_employee_name_mei), "")


        if ( (employee_number == "") || (last_name == "") || (first_name == "")) {
            val intent = android.content.Intent(this, javaClass<SettingsActivity>())
            startActivity(intent)
        }

    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.getItemId()

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            // �ݒ��ʂ��Ăяo��
            val intent = android.content.Intent(this, javaClass<SettingsActivity>())
            startActivity(intent)
            return true
        }

        return super.onOptionsItemSelected(item)
    }

}
