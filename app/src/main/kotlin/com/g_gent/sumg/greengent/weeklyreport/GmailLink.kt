package com.g_gent.sumg.greengent.weeklyreport

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import android.support.v4.app.Fragment
import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.widget.Spinner
import android.widget.TextView

/**
 * Gmailへボタン押下後の処理
 * Created by shin-macair on 2015/05/05.
 */
public class GmailLink(inputActivity : MainActivity) : View.OnClickListener {
    val activity = inputActivity

    override fun onClick(v : View) {

        if (v.getId() == R.id.button) {
            // Gmailへボタン押下した時

            // SharedPreference取得
            val prefs : SharedPreferences = activity.getSharedPreferences("Greengent", Context.MODE_PRIVATE)
            val editor : SharedPreferences.Editor = prefs.edit();

            // 週報タイトル
            var subject = (activity.findViewById(R.id.editSubject) as EditText).getText().toString()
            subject = if(subject.isNotEmpty()) subject else "【週報】"

            editor.putString("subject", subject)
            editor.apply()

            // メール送信
            val intent = Intent();

            intent.setAction(Intent.ACTION_SEND);

            // intent.setType("text/plain");
            intent.setType("message/rfc822");

            // To(暫定的にそのまま)
            intent.putExtra(Intent.EXTRA_EMAIL, array("growgent_manager@g-gent.com"));

////////////////////
            // Toの書き方
//            intent.putExtra(Intent.EXTRA_EMAIL, array("uchiyama@g-gent.com", "uchiyama+2@g-gent.com"));
            // Ccの書き方
//            intent.putExtra(Intent.EXTRA_CC, array("uchiyama+a@g-gent.com", "uchiyama+3@g-gent.com"));
            // Bccの書き方
//            intent.putExtra(Intent.EXTRA_BCC, array("uchiyama+b@g-gent.com", "uchiyama+4@g-gent.com"));
////////////////////

            // 件名をセット(toStringつけないと、件名がセットされない・・)
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            // 本文をセット
            intent.putExtra(Intent.EXTRA_TEXT, getContent(editor));
            // GMaiセット
            intent.setPackage("com.google.android.gm");

            try {
                activity.startActivity(Intent.createChooser(intent, ""));
            } catch (ex: android.content.ActivityNotFoundException) {
                ex.printStackTrace();
                Toast.makeText(activity.getApplicationContext(), "client not found", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * メール本文の作成
     *
     * @param editor SharedPreference編集用変数
     */
    private fun getContent(editor : SharedPreferences.Editor) : String {

        // プラットフォームに適した改行コードを取得
        val newLine =  System.getProperty("line.separator")
        val border = "　"

        // 勤務時間ラベル取得
        val textOfficeHours = (activity.findViewById(R.id.textOfficeHours) as TextView).getText().toString()

        // 月曜日(開始時間)
        val monStart = (activity.findViewById(R.id.editMondayStart) as EditText).getText().toString()
        // 月曜日(終了時間)
        val monEnd = (activity.findViewById(R.id.editMondayEnd) as EditText).getText().toString()
        // 月曜日(休暇種別)
        val divisionMonday = (activity.findViewById(R.id.divisionMonday) as Spinner).getSelectedItem().toString()
        // 月曜日(休暇種別)インデックス
        val divisionMondayIndex = (activity.findViewById(R.id.divisionMonday) as Spinner).getSelectedItemPosition().toInt()
        // 日付ラベル
        val mondayText = (activity.findViewById(R.id.textMon) as TextView).getText().toString();

        // 火曜日(開始時間)
        val tueStart = (activity.findViewById(R.id.editTuesdayStart) as EditText).getText().toString()
        // 火曜日(終了時間)
        val tueEnd = (activity.findViewById(R.id.editTuesdayEnd) as EditText).getText().toString()
        // 火曜日(休暇種別)
        val divisionTuesday = (activity.findViewById(R.id.divisionTuesday) as Spinner).getSelectedItem().toString()
        // 火曜日(休暇種別)インデックス
        val divisionTuesdayIndex = (activity.findViewById(R.id.divisionTuesday) as Spinner).getSelectedItemPosition().toInt()
        // 日付ラベル
        val tuesdayText = (activity.findViewById(R.id.textTue) as TextView).getText().toString();

        // 水曜日(開始時間)
        val wenStart = (activity.findViewById(R.id.editWednesdayStart) as EditText).getText().toString()
        // 水曜日(終了時間)
        val wenEnd = (activity.findViewById(R.id.editWednesdayEnd) as EditText).getText().toString()
        // 火曜日(休暇種別)
        val divisionWednesday = (activity.findViewById(R.id.divisionWednesday) as Spinner).getSelectedItem().toString()
        // 水曜日(休暇種別)インデックス
        val divisionWednesdayIndex = (activity.findViewById(R.id.divisionWednesday) as Spinner).getSelectedItemPosition().toInt()
        // 日付ラベル
        val wednesdayText = (activity.findViewById(R.id.textWed) as TextView).getText().toString();

        // 木曜日(開始時間)
        val thuStart = (activity.findViewById(R.id.editThursdayStart) as EditText).getText().toString()
        // 木曜日(終了時間)
        val thuEnd = (activity.findViewById(R.id.editThursdayEnd) as EditText).getText().toString()
        // 木曜日(休憩種別)
        val divisionThursday = (activity.findViewById(R.id.divisionThursday) as Spinner).getSelectedItem().toString()
        // 木曜日(休暇種別)インデックス
        val divisionThursdayIndex = (activity.findViewById(R.id.divisionThursday) as Spinner).getSelectedItemPosition().toInt()
        // 日付ラベル
        val thursdayText = (activity.findViewById(R.id.textThu) as TextView).getText().toString();

        // 金曜日(開始時間)
        val friStart = (activity.findViewById(R.id.editFridayStart) as EditText).getText().toString()
        // 金曜日(終了時間)
        val friEnd = (activity.findViewById(R.id.editFridayEnd) as EditText).getText().toString()
        // 金曜日(休憩種別)
        val divisionFriday = (activity.findViewById(R.id.divisionFriday) as Spinner).getSelectedItem().toString()
        // 金曜日(休暇種別)インデックス
        val divisionFridayIndex = (activity.findViewById(R.id.divisionFriday) as Spinner).getSelectedItemPosition().toInt()
        // 日付ラベル
        val fridayText = (activity.findViewById(R.id.textFri) as TextView).getText().toString();

        // 土曜日(開始時間)
        val satStart = (activity.findViewById(R.id.editSaturdayStart) as EditText).getText().toString()
        // 土曜日(終了時間)
        val satEnd = (activity.findViewById(R.id.editSaturdayEnd) as EditText).getText().toString()
        // 土曜日(休憩種別)
        val divisionSaturday = (activity.findViewById(R.id.divisionSaturday) as Spinner).getSelectedItem().toString()
        // 土曜日(休暇種別)インデックス
        val divisionSaturdayIndex = (activity.findViewById(R.id.divisionSaturday) as Spinner).getSelectedItemPosition().toInt()
        // 日付ラベル
        val saturdayText = (activity.findViewById(R.id.textSat) as TextView).getText().toString();

        // 日曜日(開始時間)
        val sunStart = (activity.findViewById(R.id.editSundayStart) as EditText).getText().toString()
        // 日曜日(終了時間)
        val sunEnd = (activity.findViewById(R.id.editSundayEnd) as EditText).getText().toString()
        // 日曜日(休憩種別)
        val divisionSunday = (activity.findViewById(R.id.divisionSunday) as Spinner).getSelectedItem().toString()
        // 曜日(休暇種別)インデックス
        val divisionSundayIndex = (activity.findViewById(R.id.divisionSunday) as Spinner).getSelectedItemPosition().toInt()
        // 日付ラベル
        val sundayText = (activity.findViewById(R.id.textSun) as TextView).getText().toString();

        // 作業内容ラベル
        val labelThisWeekContents = (activity.findViewById(R.id.thisWeekContents) as TextView).getText().toString()
        // 作業内容(今週分)
        val content = (activity.findViewById(R.id.editThisWeekContents) as EditText).getText().toString()

        // 作業内容(来週分)ラベル
        val labelNextWeekContents = (activity.findViewById(R.id.nextWeekContents) as TextView).getText().toString()
        // 作業内容(来週分)
        val nextWeekContent = (activity.findViewById(R.id.editNextWeekContents) as EditText).getText().toString()

        // 業務特記事項ラベル
        val labelSpecialNotation = (activity.findViewById(R.id.specialNotation) as TextView).getText().toString()
        // 業務特記事項
        val specialNotation = (activity.findViewById(R.id.editSpecialNotation) as EditText).getText().toString()

        // 社内業ラベル
        val labelGrowgentWork = (activity.findViewById(R.id.growgentWork) as TextView).getText().toString()
        // 社内業務
        val growgentWork = (activity.findViewById(R.id.editGrowgentWork) as EditText).getText().toString()

        // 社内活動(部活等)ラベル
        val labelExtracurricularActivities = (activity.findViewById(R.id.extracurricularActivities) as TextView).getText().toString()
        // 社内活動(部活等)
        val extraCurricularActivities = (activity.findViewById(R.id.editExtracurricularActivities) as EditText).getText().toString()

        // その他(報告、勤怠予定等)ラベル
        val labelExtras = (activity.findViewById(R.id.extras) as TextView).getText().toString()
        // その他(報告、勤怠予定等)
        val extras = (activity.findViewById(R.id.editExtras) as EditText).getText().toString()

        // メール本文
        var text : String = textOfficeHours + newLine

        // 勤務時間(月〜日曜日)
        text += if(mondayText.isNotEmpty()) mondayText + " " else ""
        text += if(monStart.isNotEmpty()) monStart + " ~ " else "- ~ "
        text += if(monEnd.isNotEmpty()) monEnd  else "-"
        text += if(divisionMonday.isNotEmpty())  " " + divisionMonday + newLine else newLine

        text += if(tuesdayText.isNotEmpty()) tuesdayText + " " else ""
        text += if(tueStart.isNotEmpty()) tueStart + " ~ " else "- ~ "
        text += if(tueEnd.isNotEmpty()) tueEnd  else "-"
        text += if(divisionTuesday.isNotEmpty())  " " + divisionTuesday + newLine else newLine

        text += if(wednesdayText.isNotEmpty()) wednesdayText + " " else ""
        text += if(wenStart.isNotEmpty()) wenStart + " ~ " else "- ~ "
        text += if(wenEnd.isNotEmpty()) wenEnd  else "-"
        text += if(divisionWednesday.isNotEmpty())  " " + divisionWednesday + newLine else newLine

        text += if(thursdayText.isNotEmpty()) thursdayText + " " else ""
        text += if(thuStart.isNotEmpty()) thuStart + " ~ " else "- ~ "
        text += if(thuEnd.isNotEmpty()) thuEnd  else "-"
        text += if(divisionThursday.isNotEmpty())  " " + divisionThursday + newLine else newLine

        text += if(fridayText.isNotEmpty()) fridayText + " " else ""
        text += if(friStart.isNotEmpty()) friStart + " ~ " else "- ~ "
        text += if(friEnd.isNotEmpty()) friEnd  else "-"
        text += if(divisionFriday.isNotEmpty())  " " + divisionFriday + newLine else newLine

        text += if(saturdayText.isNotEmpty()) saturdayText + " " else ""
        text += if(satStart.isNotEmpty()) satStart + " ~ " else "- ~ "
        text += if(satEnd.isNotEmpty()) satEnd  else "-"
        text += if(divisionSaturday.isNotEmpty())  " " + divisionSaturday + newLine else newLine

        text += if(sundayText.isNotEmpty()) sundayText + " " else ""
        text += if(sunStart.isNotEmpty()) sunStart + " ~ " else "- ~ "
        text += if(sunEnd.isNotEmpty()) sunEnd  else "-"
        text += if(divisionSunday.isNotEmpty())  " " + divisionSunday + newLine else newLine

        text += border + newLine

        // 作業内容
        text += if(content.isNotEmpty())  labelThisWeekContents + newLine + content + newLine + border + newLine else newLine
        // 来週の作業内容
        text += if(nextWeekContent.isNotEmpty()) labelNextWeekContents + newLine + nextWeekContent + newLine + border + newLine  else newLine
        // 業務特記事項
        text += if(specialNotation.isNotEmpty())  labelSpecialNotation + newLine + specialNotation + newLine + border + newLine  else newLine
        // 社内業務
        text += if(growgentWork.isNotEmpty())  labelGrowgentWork + newLine + growgentWork + newLine + border + newLine  else newLine
        // 社内活動(部活等)
        text += if(extraCurricularActivities.isNotEmpty()) labelExtracurricularActivities + newLine + extraCurricularActivities + newLine + border + newLine  else newLine
        // その他(報告、勤怠予定等)
        text += if(extras.isNotEmpty()) labelExtras + newLine + extras + newLine else newLine

        Log.d("getContent", text)

        // SharedPreferenceに格納
        editor.putString("monStart", monStart)
        editor.putString("monEnd", monEnd)
        editor.putString("divisionMonday", divisionMonday)
        editor.putInt("divisionMondayIndex", divisionMondayIndex)
        editor.putString("tueStart", tueStart)
        editor.putString("tueEnd", tueEnd)
        editor.putString("divisionTuesday", divisionTuesday)
        editor.putInt("divisionTuesdayIndex", divisionTuesdayIndex)
        editor.putString("wenStart", wenStart)
        editor.putString("wenEnd", wenEnd)
        editor.putString("divisionWednesday", divisionWednesday)
        editor.putInt("divisionWednesdayIndex", divisionWednesdayIndex)
        editor.putString("thuStart", thuStart)
        editor.putString("thuEnd", thuEnd)
        editor.putString("divisionThursday", divisionThursday)
        editor.putInt("divisionThursdayIndex", divisionThursdayIndex)
        editor.putString("friStart", friStart)
        editor.putString("friEnd", friEnd)
        editor.putString("divisionFriday", divisionFriday)
        editor.putInt("divisionFridayIndex", divisionFridayIndex)
        editor.putString("satStart", satStart)
        editor.putString("satEnd", satEnd)
        editor.putString("divisionSaturday", divisionSaturday)
        editor.putInt("divisionSaturdayIndex", divisionSaturdayIndex)
        editor.putString("sunStart", sunStart)
        editor.putString("sunEnd", sunEnd)
        editor.putString("divisionSunday", divisionSunday)
        editor.putInt("divisionSundayIndex", divisionSundayIndex)
        editor.putString("content", content)
        editor.putString("nextWeekContent", nextWeekContent)
        editor.putString("specialNotation", specialNotation)
        editor.putString("growgentWork", growgentWork)
        editor.putString("extraCurricularActivities", extraCurricularActivities)
        editor.putString("extras", extras)
        editor.apply();

        return text
    }
}
