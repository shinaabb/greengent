package com.g_gent.sumg.greengent.weeklyreport

import android.app.Activity
import android.os.Bundle

/**
 * 設定画面アクティビティ
 * 設定用のプリファレンスフラグメントを呼び出す
 * Created by marushima on 2015/05/06.
 */
public class SettingsActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // 設定用のプリファレンスフラグメント開始
        getFragmentManager().beginTransaction().replace(android.R.id.content, SettingsFragment()).commit()
    }
}