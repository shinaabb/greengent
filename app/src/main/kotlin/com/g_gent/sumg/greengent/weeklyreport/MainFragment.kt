package com.g_gent.sumg.greengent.weeklyreport

import android.app.TimePickerDialog
import android.content.Context
import android.content.SharedPreferences
import android.content.res.Resources
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import java.lang
import java.text.SimpleDateFormat
import java.util.Calendar


public class MainFragment : Fragment() {

    var firstDay : String = ""
    var lastDay : String = ""
    val startTime : String = "9 : 00"
    val endTime : String = "18 : 00"
    val editTimeArray : Array<Int> = array(R.id.editMondayStart, R.id.editMondayEnd, R.id.editTuesdayStart, R.id.editTuesdayEnd,
            R.id.editWednesdayStart, R.id.editWednesdayEnd, R.id.editThursdayStart, R.id.editThursdayEnd,
            R.id.editFridayStart, R.id.editFridayEnd , R.id.editSaturdayStart, R.id.editSaturdayEnd,
            R.id.editSundayStart, R.id.editSundayEnd)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val rootView = inflater.inflate(R.layout.fragment_main, container, false)

        // 日付デフォルト値設定
        setWeekday(rootView)
        // メールタイトル設定
        setDefaultSubject(rootView)
        // TimePickerを設定
        setTimePickerDialogListener(rootView)
        // 前回設定を取得
        getPreviousContent(rootView)

        // Gmailへボタン押下時のリスナー登録
        rootView.findViewById(R.id.button).setOnClickListener(GmailLink(getActivity() as MainActivity));

        return rootView
    }

    override fun onResume() {
        super.onResume()
        setDefaultSubject(getView())
    }


    fun setDefaultSubject(rootView : View) {

        val subject = rootView.findViewById(R.id.editSubject) as EditText

        // 設定値を取得
        val res = getResources() as Resources
        val preferences = PreferenceManager.getDefaultSharedPreferences(rootView.getContext()) as SharedPreferences
        val employee_number = preferences.getString(res.getString(R.string.pref_key_employee_number), "")
        val last_name = preferences.getString(res.getString(R.string.pref_key_employee_name_sei), "")
        val first_name = preferences.getString(res.getString(R.string.pref_key_employee_name_mei), "")

        subject.setText("[週報]${this.firstDay}~${this.lastDay} G${employee_number} ${last_name} ${first_name}")
    }

    fun setWeekday(rootView : View) {

        val array = array (R.id.textMon, R.id.textTue, R.id.textWed, R.id.textThu, R.id.textFri, R.id.textSat, R.id.textSun)
        val c = Calendar.getInstance()
        val sdf = SimpleDateFormat("M/d(E)")

        if(c.get(Calendar.DAY_OF_WEEK) < 6) {
            c.add(Calendar.WEEK_OF_MONTH, -1)
        }
        c.set(Calendar.DAY_OF_WEEK, 2)
        this.firstDay = sdf.format(c.getTime())

        for (e in array) {
            val text = rootView.findViewById(e) as TextView
            text.setText(sdf.format(c.getTime()))
            c.add(Calendar.DAY_OF_WEEK, 1)
        }
        c.add(Calendar.DAY_OF_WEEK, -1)
        this.lastDay = sdf.format(c.getTime())

    }


    fun setTimePickerDialogListener(rootView : View) {

        val c = Calendar.getInstance()

        for (e in editTimeArray) {
            val edittext = rootView.findViewById(e) as EditText
            edittext.setOnFocusChangeListener { (view, b) ->
                if (b == true ){
                    TimePickerDialog(rootView.getContext(),
                            TimePickerDialog.OnTimeSetListener { (timePicker, i1, i2) ->
                                edittext.setText("${i1} : ${lang.String.format("%02d",i2)}")
                            }
                            , c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), true)
                            .show()
                }
            }
        }

    }

    fun getPreviousContent(rootView : View) {

        val prefs : SharedPreferences = rootView.getContext().getSharedPreferences("Greengent", Context.MODE_PRIVATE)

        // 月曜日設定
        var edittext = rootView.findViewById(R.id.editMondayStart) as EditText
        edittext.setText(prefs.getString("monStart", startTime))
        edittext = rootView.findViewById(R.id.editMondayEnd) as EditText
        edittext.setText(prefs.getString("monEnd", endTime))
        var spinner = rootView.findViewById(R.id.divisionMonday) as Spinner
        spinner.setSelection(prefs.getInt("divisionMondayIndex", 0))

        // 火曜日設定
        edittext = rootView.findViewById(R.id.editTuesdayStart) as EditText
        edittext.setText(prefs.getString("tueStart", startTime))
        edittext = rootView.findViewById(R.id.editTuesdayEnd) as EditText
        edittext.setText(prefs.getString("tueEnd", endTime))
        spinner = rootView.findViewById(R.id.divisionTuesday) as Spinner
        spinner.setSelection(prefs.getInt("divisionTuesdayIndex", 0))

        // 水曜日設定
        edittext = rootView.findViewById(R.id.editWednesdayStart) as EditText
        edittext.setText(prefs.getString("wenStart", startTime))
        edittext = rootView.findViewById(R.id.editWednesdayEnd) as EditText
        edittext.setText(prefs.getString("wenEnd", endTime))
        spinner = rootView.findViewById(R.id.divisionWednesday) as Spinner
        spinner.setSelection(prefs.getInt("divisionWednesdayIndex", 0))

        // 木曜日設定
        edittext = rootView.findViewById(R.id.editThursdayStart) as EditText
        edittext.setText(prefs.getString("thuStart", startTime))
        edittext = rootView.findViewById(R.id.editThursdayEnd) as EditText
        edittext.setText(prefs.getString("thuEnd", endTime))
        spinner = rootView.findViewById(R.id.divisionThursday) as Spinner
        spinner.setSelection(prefs.getInt("divisionThursdayIndex", 0))

        // 金曜日設定
        edittext = rootView.findViewById(R.id.editFridayStart) as EditText
        edittext.setText(prefs.getString("friStart", startTime))
        edittext = rootView.findViewById(R.id.editFridayEnd) as EditText
        edittext.setText(prefs.getString("friEnd", endTime))
        spinner = rootView.findViewById(R.id.divisionFriday) as Spinner
        spinner.setSelection(prefs.getInt("divisionFridayIndex", 0))

        // 土曜日設定
        edittext = rootView.findViewById(R.id.editSaturdayStart) as EditText
        edittext.setText(prefs.getString("satStart", ""))
        edittext = rootView.findViewById(R.id.editSaturdayEnd) as EditText
        edittext.setText(prefs.getString("satEnd", ""))
        spinner = rootView.findViewById(R.id.divisionSaturday) as Spinner
        spinner.setSelection(prefs.getInt("divisionSaturdayIndex", 0))

        // 日曜日設定
        edittext = rootView.findViewById(R.id.editSundayStart) as EditText
        edittext.setText(prefs.getString("sunStart", ""))
        edittext = rootView.findViewById(R.id.editSundayEnd) as EditText
        edittext.setText(prefs.getString("sunEnd", ""))
        spinner = rootView.findViewById(R.id.divisionSunday) as Spinner
        spinner.setSelection(prefs.getInt("divisionSundayIndex", 0))


        // 作業
        edittext = rootView.findViewById(R.id.editThisWeekContents) as EditText
        edittext.setText(prefs.getString("content", ""))

        // 翌週
        edittext = rootView.findViewById(R.id.editNextWeekContents) as EditText
        edittext.setText(prefs.getString("nextWeekContent", ""))

        // 特記
        edittext = rootView.findViewById(R.id.editSpecialNotation) as EditText
        edittext.setText(prefs.getString("specialNotation", ""))

        // 社内業務
        edittext = rootView.findViewById(R.id.editGrowgentWork) as EditText
        edittext.setText(prefs.getString("growgentWork", ""))

        // 社内活動
        edittext = rootView.findViewById(R.id.editExtracurricularActivities) as EditText
        edittext.setText(prefs.getString("extraCurricularActivities", ""))

        // その他
        edittext = rootView.findViewById(R.id.editExtras) as EditText
        edittext.setText(prefs.getString("extras", ""))

    }


}
