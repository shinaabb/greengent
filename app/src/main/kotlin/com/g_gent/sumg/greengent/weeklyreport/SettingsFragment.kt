package com.g_gent.sumg.greengent.weeklyreport

import android.os.Bundle
import android.preference.EditTextPreference
import android.preference.Preference
import android.preference.PreferenceFragment

/**
 * 設定画面用のフラグメント
 * Created by marushima on 2015/05/06.
 */
public class SettingsFragment : PreferenceFragment() {

    /**
     * フラグメント作成
     * @param savedInstanceState
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // 設定画面構成用のXMLを設定
        addPreferencesFromResource(R.xml.preference)

        // 社員番号の初期設定
        setSummary(getText(R.string.pref_key_employee_number))
        // 姓の初期設定
        setSummary(getText(R.string.pref_key_employee_name_sei))
        // 名の初期設定
        setSummary(getText(R.string.pref_key_employee_name_mei))
    }

    /**
     * プリファレンスのサマリーを初期設定/更新する
     * @param key 共有プリファレンスの対象キー
     */
    private fun setSummary(key: CharSequence) {
        // 共有プリファレンスから対象プリファレンスを取得
        val pref = findPreference(key)

        // テキストボックスのプレファレンスを取得
        val editTextPref = getPreferenceScreen().findPreference(key) as EditTextPreference
        // サマリーを設定
        editTextPref.setSummary(editTextPref.getText())

        // プリファレンス変更時のリスナーを設定
        pref.setOnPreferenceChangeListener(object : Preference.OnPreferenceChangeListener {
            /**
             * プレファレンス変更時のサマリー設定イベント定義
             * @param pref
             * *
             * @param obj
             * *
             * @return boolean
             */
            override fun onPreferenceChange(pref: Preference, obj: Any?): Boolean {
                if (obj != null) {
                    // サマリーを設定
                    pref.setSummary(obj as CharSequence)
                    return true
                }
                return false
            }
        })
    }
}